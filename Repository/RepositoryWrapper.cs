﻿using Contracts;
using Contracts.Interfaces;
using Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repositoryContext;
        private IContactRepository _contact;
        private IContactAddressRepository _contactAddress;
        private IContactPhoneRepository _contactPhone;
        private IContactEmailRepository _contactEmail;
        private ITaskRepository _task;
        private IActivityRepository _activity;
        private ILockedItemRepository _lockedItem;
        private IContactAdditionalInfoRepository _contactAdditionalInfo;

        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public IContactRepository Contact
        {
            get
            {
                if (_contact == null)
                {
                    _contact = new ContactRepository(_repositoryContext);
                }
                return _contact;
            }
        }

        public IContactAddressRepository ContactAddress
        {
            get
            {
                if (_contactAddress == null)
                {
                    _contactAddress = new ContactAddressRepository(_repositoryContext);
                }
                return _contactAddress;
            }
        }

        public IContactPhoneRepository ContactPhone
        {
            get
            {
                if (_contactPhone == null)
                {
                    _contactPhone = new ContactPhoneRepository(_repositoryContext);
                }
                return _contactPhone;
            }
        }

        public IContactEmailRepository ContactEmail
        {
            get
            {
                if (_contactEmail == null)
                {
                    _contactEmail = new ContactEmailRepository(_repositoryContext);
                }
                return _contactEmail;
            }
        }

        public IActivityRepository Activity
        {
            get
            {
                if (_activity == null)
                {
                    _activity = new ActivityRepository(_repositoryContext);
                }
                return _activity;
            }
        }

        public ITaskRepository Task
        {
            get
            {
                if (_task == null)
                {
                    _task = new TaskRepository(_repositoryContext);
                }
                return _task;
            }
        }
        public ILockedItemRepository LockedItem
        {
            get
            {
                if (_lockedItem == null)
                {
                    _lockedItem = new LockedItemRepository(_repositoryContext);
                }
                return _lockedItem;
            }
        }

        public IContactAdditionalInfoRepository ContactAdditionalInfo
        {
            get
            {
                if (_contactAdditionalInfo == null)
                {
                    _contactAdditionalInfo = new ContactAdditionalInfoRepository(_repositoryContext);
                }
                return _contactAdditionalInfo;
            }
        }

        public async Task SaveAsync()
        {
            await _repositoryContext.SaveChangesAsync();
        }
    }
}
