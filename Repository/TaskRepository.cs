﻿using Contracts.Interfaces;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class TaskRepository : RepositoryBase<Entities.Models.Task>, ITaskRepository
    {
        public TaskRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {

        }
        public void CreateTask(Entities.Models.Task task)
        {
            Create(task);
        }

        public void DeleteTask(Entities.Models.Task task)
        {
            Delete(task);
        }

        public async Task<Entities.Models.Task> GetTaskByIdAsync(Guid id)
        {
            return await FindByCondition(c => c.Id.Equals(id)).FirstOrDefaultAsync();
        }

        public void UpdateTask(Entities.Models.Task task)
        {
            Update(task);
        }

        public async Task<IEnumerable<Entities.Models.Task>> GetTasksByContactIdAsync(Guid id)
        {
            return await FindByCondition(t => t.ContactId.Equals(id)).ToListAsync();
        }

        public async Task<IEnumerable<Entities.Models.Task>> GetAllTasksAsync()
        {
            return await FindAll()
                .Include(act => act.Activities)
                .Include(contact => contact.Contact)
                .ToListAsync();
        }
    }
}
