﻿using Contracts.Interfaces;
using Entities;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public class ContactEmailRepository : RepositoryBase<ContactEmail>, IContactEmailRepository
    {
        public ContactEmailRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
            
        }
        public void CreateContactEmailsAsync(IEnumerable<ContactEmail> contactEmails)
        {
            CreateContactEmailsAsync(contactEmails);
        }
    }
}
