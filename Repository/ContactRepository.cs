﻿using Contracts.Interfaces;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class ContactRepository : RepositoryBase<Contact>, IContactRepository
    {
        public ContactRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {

        }

        public async Task<IEnumerable<Contact>> GetAllContactsAsync()
        {
            return await FindAll().OrderBy(c => c.FirstName)
                .Include(locked => locked.LockedItem)
                .Include(address => address.ContactAddresses)
                .Include(additional => additional.ContactAdditionalInfo)
                .ToListAsync();
        }

        public async Task<Contact> GetContactByIdAsync(Guid id)
        {
            return await FindByCondition(c => c.Id.Equals(id)).FirstOrDefaultAsync();
        }

        public void CreateContact(Contact contact)
        {
            Create(contact);
        }

        public void DeleteContact(Contact contact)
        {
            Delete(contact);
        }

        public void UpdateContact(Contact contact)
        {
            Update(contact);
        }
    }
}
