﻿using Contracts.Interfaces;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Repository
{
    public class ContactAdditionalInfoRepository : RepositoryBase<ContactAdditionalInfo>, IContactAdditionalInfoRepository
    {
        public ContactAdditionalInfoRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }
        public void CreateContactAdditionalInfo(ContactAdditionalInfo contactAdditionalInfo)
        {
            Create(contactAdditionalInfo);
        }

        public void DeleteContactAdditionalInfo(ContactAdditionalInfo contactAdditionalInfo)
        {
            Delete(contactAdditionalInfo);
        }

        public async Task<ContactAdditionalInfo> GetContactAdditionalInfoByIdAsync(Guid id)
        {
            return await FindByCondition(c=>c.Equals(id)).FirstOrDefaultAsync();
        }

        public void UpdateContactAdditionalInfo(ContactAdditionalInfo contactAdditionalInfo)
        {
            Update(contactAdditionalInfo);
        }
    }
}
