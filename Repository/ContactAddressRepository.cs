﻿using Contracts.Interfaces;
using Entities;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public class ContactAddressRepository : RepositoryBase<ContactAddress>, IContactAddressRepository
    {
        public ContactAddressRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }
        public void CreateContactAddress(ContactAddress contactAddress)
        {
            Create(contactAddress);
        }
    }
}
