﻿using Contracts.Interfaces;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    class LockedItemRepository : RepositoryBase<LockedItem>, ILockedItemRepository
    {
        public LockedItemRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {

        }
        public async Task<IEnumerable<LockedItem>> GetLockedItems()
        {
            return await FindAll().ToListAsync();
        }
        public void CreateLockedItem(LockedItem lockedItem)
        {
            Create(lockedItem);
        }

        public void DeleteLockedItem(LockedItem lockedItem)
        {
            Delete(lockedItem);
        }
    }
}
