﻿using Contracts.Interfaces;
using Entities;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public class ContactPhoneRepository : RepositoryBase<ContactPhone>, IContactPhoneRepository
    {
        public ContactPhoneRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }

        public void CreateContactPhonesAsync(IEnumerable<ContactPhone> contactPhones)
        {
            CreateContactPhonesAsync(contactPhones);
        }
    }
}
