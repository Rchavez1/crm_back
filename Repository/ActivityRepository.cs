﻿using Contracts.Interfaces;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class ActivityRepository : RepositoryBase<Activity>, IActivityRepository
    {
        public ActivityRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }
        public void CreateActivity(Activity activity)
        {
            Create(activity);
        }

        public void DeleteActivity(Activity activity)
        {
            Delete(activity);
        }

        public async Task<Activity> GetActivityByIdAsync(Guid id)
        {
            return await FindByCondition(a => a.Id.Equals(id)).FirstOrDefaultAsync();
        }

        public void UpdateActivity(Activity activity)
        {
            Update(activity);
        }
    }
}
