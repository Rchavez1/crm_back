﻿using AutoMapper;
using Entities.DataTransferObjects;
using Entities.Models;
using Task = Entities.Models.Task;

namespace crmback
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<Contact, ContactDto>();
            CreateMap<CreateContactDto, Contact>();
            CreateMap<EditContactDto, Contact>();
            CreateMap<Task, TaskDto>();
            CreateMap<TaskDto, Task>();
            CreateMap<ContactAddress, CreateContactAddressDto>();
            CreateMap<ContactEmail, CreateEmailDto>();
            CreateMap<ContactPhone, CreatePhoneDto>();
            CreateMap<CreateContactAddressDto, ContactAddress>();
            CreateMap<CreateContactAdditionalInfoDto, ContactAdditionalInfo>();
            CreateMap<ContactAdditionalInfo, CreateContactAdditionalInfoDto>();
            CreateMap<UpdateTaskDto, Task>();
        }
    }
}
