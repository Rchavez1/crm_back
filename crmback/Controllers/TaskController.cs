﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace crmback.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private IRepositoryWrapper _repositoryWrapper;
        private readonly ILogger<TaskController> _logger;
        private IMapper _mapper;

        public TaskController(IRepositoryWrapper repositoryWrapper, ILogger<TaskController> logger, IMapper mapper)
        {
            _repositoryWrapper = repositoryWrapper;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllTasks()
        {
            try
            {
                var tasks = await _repositoryWrapper.Task.GetAllTasksAsync();
                _logger.LogInformation($"Returned all tasks from database.");

                var tasksResult = _mapper.Map<IEnumerable<TaskDto>>(tasks);
                return Ok(tasksResult);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllTasks action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("GetUserTasksById/{id}")]
        public async Task<IActionResult> GetUserTasksById(Guid id)
        {
            try
            {
                var contactTasks = await _repositoryWrapper.Task.GetTasksByContactIdAsync(id);
                _logger.LogInformation($"Returned all contact tasks from database.");

                var tasksResult = _mapper.Map<IEnumerable<TaskDto>>(contactTasks);
                return Ok(tasksResult);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetUserTasksById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateTask([FromBody]TaskDto task)
        {
            try
            {
                if (task == null)
                {
                    _logger.LogError("task object sent from client is null.");
                    return BadRequest("task is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid task object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var taskEntity = _mapper.Map<Entities.Models.Task>(task);
                _repositoryWrapper.Task.CreateTask(taskEntity);
                await _repositoryWrapper.SaveAsync();
                var createdTask = _mapper.Map<TaskDto>(taskEntity);
                return Created("CreateTask", createdTask);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateTask action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateTask(Guid id, [FromBody]UpdateTaskDto task)
        {
            try
            {
                if (task == null)
                {
                    _logger.LogError("Task object sent from client is null.");
                    return BadRequest("Task object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Task object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var taskEntity = await _repositoryWrapper.Task.GetTaskByIdAsync(id);
                if (taskEntity == null)
                {
                    _logger.LogError($"Task with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _mapper.Map(task, taskEntity);
                _repositoryWrapper.Task.UpdateTask(taskEntity);
                await _repositoryWrapper.SaveAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateTask action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}