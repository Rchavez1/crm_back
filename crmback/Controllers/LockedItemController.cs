﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace crmback.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LockedItemController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;
        private readonly ILogger<LockedItemController> _logger;
        private IMapper _mapper;

        public LockedItemController(IRepositoryWrapper repositoryWrapper, ILogger<LockedItemController> logger, IMapper mapper)
        {
            _repoWrapper = repositoryWrapper;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> CreateLockedItem([FromBody]LockedItem lockedItem)
        {
            try
            {
                if (lockedItem == null)
                {
                    _logger.LogError("LockedItem object sent from client is null.");
                    return BadRequest("Contact is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid LockedItem object sent from client.");
                    return BadRequest("Invalid model object");
                }

                _repoWrapper.LockedItem.CreateLockedItem(lockedItem);
                // temp insert for contacts, change to get table parameter from locked item object
                var contact = await _repoWrapper.Contact.FindByCondition(c => c.Id.Equals(lockedItem.ItemId)).FirstOrDefaultAsync();
                contact.LockedItem = lockedItem;
                _repoWrapper.Contact.UpdateContact(contact);
                await _repoWrapper.SaveAsync();
                return Created("CreateLockedItem", lockedItem);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateLockedItem action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete]
        public async Task<IActionResult> RemoveItemLock([FromBody]LockedItem lockedItem)
        {
            try
            {
                if (lockedItem == null)
                {
                    _logger.LogError("LockedItem object sent from client is null.");
                    return BadRequest("Contact is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid LockedItem object sent from client.");
                    return BadRequest("Invalid model object");
                }

                _repoWrapper.LockedItem.DeleteLockedItem(lockedItem);
                
                await _repoWrapper.SaveAsync();

                var lockedItems = await _repoWrapper.LockedItem.GetLockedItems();
                return Ok(lockedItems);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateLockedItem action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}