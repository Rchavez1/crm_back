﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace crmback.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactAddressController : ControllerBase
    {
        private IRepositoryWrapper _repositoryWrapper;
        private readonly ILogger<ContactAddress> _logger;
        private IMapper _mapper;

        public ContactAddressController(IRepositoryWrapper repositoryWrapper, ILogger<ContactAddress> logger, IMapper mapper)
        {
            _repositoryWrapper = repositoryWrapper;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> CreateContactAddress([FromBody]CreateContactAddressDto contactAddress)
        {
            try
            {
                if (contactAddress == null)
                {
                    _logger.LogError("ContactAddress object sent from client is null.");
                    return BadRequest("ContactAddress is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid ContactAddress object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var contactAddressEntity = _mapper.Map<ContactAddress>(contactAddress);
                _repositoryWrapper.ContactAddress.CreateContactAddress(contactAddressEntity);
                
                await _repositoryWrapper.SaveAsync();

                var createdContactAddress = _mapper.Map<CreateContactAddressDto>(contactAddressEntity);
                return Created("CreateContactAddress", createdContactAddress);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateContact action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}