﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace crmback.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactAdditionalInfoController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;
        private readonly ILogger<ContactController> _logger;
        private IMapper _mapper;
        public ContactAdditionalInfoController(ILogger<ContactController> logger, IRepositoryWrapper repositoryWrapper, IMapper mapper)
        {
            _logger = logger;
            _repoWrapper = repositoryWrapper;
            _mapper = mapper;
        }
        [HttpPost]
        public async Task<IActionResult> CreateContactAdditionalInfo([FromBody]CreateContactAdditionalInfoDto contactAdditionalInfo)
        {
            try
            {
                if (contactAdditionalInfo == null)
                {
                    _logger.LogError("ContactAdditionalInfo object sent from client is null.");
                    return BadRequest("ContactAdditionalInfo is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid ContactAdditionalInfo object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var contactAdditionalInfoEntity = _mapper.Map<ContactAdditionalInfo>(contactAdditionalInfo);
                _repoWrapper.ContactAdditionalInfo.CreateContactAdditionalInfo(contactAdditionalInfoEntity);
                await _repoWrapper.SaveAsync();
                var createdContactAdditionalInfo = _mapper.Map<CreateContactAdditionalInfoDto>(contactAdditionalInfoEntity);
                return Created("CreateContactAdditionalInfo", createdContactAdditionalInfo);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateContactAdditionalInfo action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}