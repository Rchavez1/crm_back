﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace crmback.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;
        private readonly ILogger<ContactController> _logger;
        private IMapper _mapper;

        public ContactController(ILogger<ContactController> logger, IRepositoryWrapper repositoryWrapper, IMapper mapper)
        {
            _logger = logger;
            _repoWrapper = repositoryWrapper;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllContacts()
        {
            try
            {
                var contacts = await _repoWrapper.Contact.GetAllContactsAsync();
                _logger.LogInformation($"Returned all contacts from database.");

                var contactsResult = _mapper.Map<IEnumerable<ContactDto>>(contacts);
                return Ok(contactsResult);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllContacts action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateContact([FromBody]CreateContactDto contact)
        {
            try
            {
                if (contact == null)
                {
                    _logger.LogError("Contact object sent from client is null.");
                    return BadRequest("Contact is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Contact object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var contactEntity = _mapper.Map<Contact>(contact);
                _repoWrapper.Contact.CreateContact(contactEntity);
                await _repoWrapper.SaveAsync();
                var createdContact = _mapper.Map<ContactDto>(contactEntity);
                return Created("CreateContact", createdContact);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateContact action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateContact(Guid id, [FromBody]EditContactDto contact)
        {
            try
            {
                if (contact == null)
                {
                    _logger.LogError("Contact object sent from client is null.");
                    return BadRequest("Contact object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid contact object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var contactEntity = await _repoWrapper.Contact.GetContactByIdAsync(id);
                if (contactEntity == null)
                {
                    _logger.LogError($"Contact with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _mapper.Map(contact, contactEntity);
                _repoWrapper.Contact.UpdateContact(contactEntity);
                await _repoWrapper.SaveAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateContact action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}