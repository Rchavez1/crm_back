﻿using crmback.SignalR.Hubs.Clients;
using crmback.SignalR.Models;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace crmback.SignalR.Hubs
{
    public class StatusHub : Hub<IStatusClient>
    {
        public async Task SendMessage(StatusMessage message)
        {
            await Clients.All.ReceiveMessage(message);
        }
    }
}
