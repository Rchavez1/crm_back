﻿using crmback.SignalR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace crmback.SignalR.Hubs.Clients
{
    public interface IStatusClient
    {
        Task ReceiveMessage(StatusMessage statusMessage);
    }
}
