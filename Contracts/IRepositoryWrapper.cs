﻿using Contracts.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IRepositoryWrapper
    {
        IContactRepository Contact { get; }
        IContactAddressRepository ContactAddress { get; }
        IContactPhoneRepository ContactPhone { get; }
        IContactEmailRepository ContactEmail { get; }
        ITaskRepository Task { get; }
        IActivityRepository Activity { get; }
        ILockedItemRepository LockedItem { get; }
        IContactAdditionalInfoRepository ContactAdditionalInfo { get; }
        Task SaveAsync();
    }
}
