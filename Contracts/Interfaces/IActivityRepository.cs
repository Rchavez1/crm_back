﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Interfaces
{
    public interface IActivityRepository
    {
        Task<Activity> GetActivityByIdAsync(Guid id);
        void CreateActivity(Activity activity);
        void UpdateActivity(Activity activity);
        void DeleteActivity(Activity activity);
    }
}
