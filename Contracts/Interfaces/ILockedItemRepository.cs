﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Interfaces
{
    public interface ILockedItemRepository
    {
        Task<IEnumerable<LockedItem>> GetLockedItems();
        void DeleteLockedItem(LockedItem lockedItem);
        void CreateLockedItem(LockedItem lockedItem);
    }
}
