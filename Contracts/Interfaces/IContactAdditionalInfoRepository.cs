﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Interfaces
{
    public interface IContactAdditionalInfoRepository : IRepositoryBase<ContactAdditionalInfo>
    {
        Task<ContactAdditionalInfo> GetContactAdditionalInfoByIdAsync(Guid id);
        void CreateContactAdditionalInfo(ContactAdditionalInfo contactAdditionalInfo);
        void UpdateContactAdditionalInfo(ContactAdditionalInfo contactAdditionalInfo);
        void DeleteContactAdditionalInfo(ContactAdditionalInfo contactAdditionalInfo);
    }
}
