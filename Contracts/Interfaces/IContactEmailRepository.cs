﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts.Interfaces
{
    public interface IContactEmailRepository : IRepositoryBase<ContactEmail>
    {
        void CreateContactEmailsAsync(IEnumerable<ContactEmail> contactEmails);
    }
}
