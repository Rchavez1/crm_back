﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Interfaces
{
    public interface ITaskRepository : IRepositoryBase<Entities.Models.Task>
    {
        Task<Entities.Models.Task> GetTaskByIdAsync(Guid id);
        void CreateTask(Entities.Models.Task task);
        void UpdateTask(Entities.Models.Task task);
        void DeleteTask(Entities.Models.Task task);
        Task<IEnumerable<Entities.Models.Task>> GetTasksByContactIdAsync(Guid id);
        Task<IEnumerable<Entities.Models.Task>> GetAllTasksAsync();
    }
}
