﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts.Interfaces
{
    public interface IContactPhoneRepository : IRepositoryBase<ContactPhone>
    {
        void CreateContactPhonesAsync(IEnumerable<ContactPhone> contactPhones);
    }
}
