﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class ContactAddressRelation2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_contactAddress_contact_ContactId",
                table: "contactAddress");

            migrationBuilder.AddForeignKey(
                name: "FK_contactAddress_contact_ContactId",
                table: "contactAddress",
                column: "ContactId",
                principalTable: "contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_contactAddress_contact_ContactId",
                table: "contactAddress");

            migrationBuilder.AddForeignKey(
                name: "FK_contactAddress_contact_ContactId",
                table: "contactAddress",
                column: "ContactId",
                principalTable: "contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
