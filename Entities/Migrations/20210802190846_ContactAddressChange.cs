﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class ContactAddressChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_contactEmail_contact_ContactId",
                table: "contactEmail");

            migrationBuilder.DropForeignKey(
                name: "FK_contactPhone_contact_ContactId",
                table: "contactPhone");

            migrationBuilder.DropIndex(
                name: "IX_contactPhone_ContactId",
                table: "contactPhone");

            migrationBuilder.DropIndex(
                name: "IX_contactEmail_ContactId",
                table: "contactEmail");

            migrationBuilder.DropColumn(
                name: "ContactId",
                table: "contactPhone");

            migrationBuilder.DropColumn(
                name: "ContactId",
                table: "contactEmail");

            migrationBuilder.AddColumn<Guid>(
                name: "ContactAddressId",
                table: "contactPhone",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ContactAddressId",
                table: "contactEmail",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_contactPhone_ContactAddressId",
                table: "contactPhone",
                column: "ContactAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_contactEmail_ContactAddressId",
                table: "contactEmail",
                column: "ContactAddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_contactEmail_contactAddress_ContactAddressId",
                table: "contactEmail",
                column: "ContactAddressId",
                principalTable: "contactAddress",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_contactPhone_contactAddress_ContactAddressId",
                table: "contactPhone",
                column: "ContactAddressId",
                principalTable: "contactAddress",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_contactEmail_contactAddress_ContactAddressId",
                table: "contactEmail");

            migrationBuilder.DropForeignKey(
                name: "FK_contactPhone_contactAddress_ContactAddressId",
                table: "contactPhone");

            migrationBuilder.DropIndex(
                name: "IX_contactPhone_ContactAddressId",
                table: "contactPhone");

            migrationBuilder.DropIndex(
                name: "IX_contactEmail_ContactAddressId",
                table: "contactEmail");

            migrationBuilder.DropColumn(
                name: "ContactAddressId",
                table: "contactPhone");

            migrationBuilder.DropColumn(
                name: "ContactAddressId",
                table: "contactEmail");

            migrationBuilder.AddColumn<Guid>(
                name: "ContactId",
                table: "contactPhone",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ContactId",
                table: "contactEmail",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_contactPhone_ContactId",
                table: "contactPhone",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_contactEmail_ContactId",
                table: "contactEmail",
                column: "ContactId");

            migrationBuilder.AddForeignKey(
                name: "FK_contactEmail_contact_ContactId",
                table: "contactEmail",
                column: "ContactId",
                principalTable: "contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_contactPhone_contact_ContactId",
                table: "contactPhone",
                column: "ContactId",
                principalTable: "contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
