﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class ContactAdditionalInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ContactAdditionalInfoId",
                table: "contact",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "contactAdditionalInfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    WebsiteURL = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FacebookURL = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    InstagramURL = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_contactAdditionalInfo", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_contact_ContactAdditionalInfoId",
                table: "contact",
                column: "ContactAdditionalInfoId");

            migrationBuilder.AddForeignKey(
                name: "FK_contact_contactAdditionalInfo_ContactAdditionalInfoId",
                table: "contact",
                column: "ContactAdditionalInfoId",
                principalTable: "contactAdditionalInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_contact_contactAdditionalInfo_ContactAdditionalInfoId",
                table: "contact");

            migrationBuilder.DropTable(
                name: "contactAdditionalInfo");

            migrationBuilder.DropIndex(
                name: "IX_contact_ContactAdditionalInfoId",
                table: "contact");

            migrationBuilder.DropColumn(
                name: "ContactAdditionalInfoId",
                table: "contact");
        }
    }
}
