﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class ContactCascadeDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_contact_LockedItems_LockedItemId",
                table: "contact");

            migrationBuilder.DropIndex(
                name: "IX_contact_LockedItemId",
                table: "contact");

            migrationBuilder.DropColumn(
                name: "LockedItemId",
                table: "contact");

            migrationBuilder.CreateIndex(
                name: "IX_LockedItems_ItemId",
                table: "LockedItems",
                column: "ItemId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_LockedItems_contact_ItemId",
                table: "LockedItems",
                column: "ItemId",
                principalTable: "contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LockedItems_contact_ItemId",
                table: "LockedItems");

            migrationBuilder.DropIndex(
                name: "IX_LockedItems_ItemId",
                table: "LockedItems");

            migrationBuilder.AddColumn<Guid>(
                name: "LockedItemId",
                table: "contact",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_contact_LockedItemId",
                table: "contact",
                column: "LockedItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_contact_LockedItems_LockedItemId",
                table: "contact",
                column: "LockedItemId",
                principalTable: "LockedItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
