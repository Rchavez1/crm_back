﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class ContactAdditionalInfoRelationId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_contactAdditionalInfo_contact_ContactId1",
                table: "contactAdditionalInfo");

            migrationBuilder.DropIndex(
                name: "IX_contactAdditionalInfo_ContactId1",
                table: "contactAdditionalInfo");

            migrationBuilder.DropColumn(
                name: "ContactId1",
                table: "contactAdditionalInfo");

            migrationBuilder.DropColumn(
                name: "ContactId2",
                table: "contactAdditionalInfo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ContactId1",
                table: "contactAdditionalInfo",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ContactId2",
                table: "contactAdditionalInfo",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_contactAdditionalInfo_ContactId1",
                table: "contactAdditionalInfo",
                column: "ContactId1");

            migrationBuilder.AddForeignKey(
                name: "FK_contactAdditionalInfo_contact_ContactId1",
                table: "contactAdditionalInfo",
                column: "ContactId1",
                principalTable: "contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
