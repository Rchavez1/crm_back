﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class LockedItemsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Activities_Tasks_TaskId",
                table: "Activities");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_contact_ContactId",
                table: "Tasks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tasks",
                table: "Tasks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Activities",
                table: "Activities");

            migrationBuilder.RenameTable(
                name: "Tasks",
                newName: "task");

            migrationBuilder.RenameTable(
                name: "Activities",
                newName: "activity");

            migrationBuilder.RenameIndex(
                name: "IX_Tasks_ContactId",
                table: "task",
                newName: "IX_task_ContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Activities_TaskId",
                table: "activity",
                newName: "IX_activity_TaskId");

            migrationBuilder.AddColumn<Guid>(
                name: "LockedItemId",
                table: "contact",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_task",
                table: "task",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_activity",
                table: "activity",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "LockedItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    User = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ItemId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LockedItems", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_contact_LockedItemId",
                table: "contact",
                column: "LockedItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_activity_task_TaskId",
                table: "activity",
                column: "TaskId",
                principalTable: "task",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_contact_LockedItems_LockedItemId",
                table: "contact",
                column: "LockedItemId",
                principalTable: "LockedItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_task_contact_ContactId",
                table: "task",
                column: "ContactId",
                principalTable: "contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_activity_task_TaskId",
                table: "activity");

            migrationBuilder.DropForeignKey(
                name: "FK_contact_LockedItems_LockedItemId",
                table: "contact");

            migrationBuilder.DropForeignKey(
                name: "FK_task_contact_ContactId",
                table: "task");

            migrationBuilder.DropTable(
                name: "LockedItems");

            migrationBuilder.DropIndex(
                name: "IX_contact_LockedItemId",
                table: "contact");

            migrationBuilder.DropPrimaryKey(
                name: "PK_task",
                table: "task");

            migrationBuilder.DropPrimaryKey(
                name: "PK_activity",
                table: "activity");

            migrationBuilder.DropColumn(
                name: "LockedItemId",
                table: "contact");

            migrationBuilder.RenameTable(
                name: "task",
                newName: "Tasks");

            migrationBuilder.RenameTable(
                name: "activity",
                newName: "Activities");

            migrationBuilder.RenameIndex(
                name: "IX_task_ContactId",
                table: "Tasks",
                newName: "IX_Tasks_ContactId");

            migrationBuilder.RenameIndex(
                name: "IX_activity_TaskId",
                table: "Activities",
                newName: "IX_Activities_TaskId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tasks",
                table: "Tasks",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Activities",
                table: "Activities",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Activities_Tasks_TaskId",
                table: "Activities",
                column: "TaskId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_contact_ContactId",
                table: "Tasks",
                column: "ContactId",
                principalTable: "contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
