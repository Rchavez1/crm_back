﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("contactAddress")]
    public class ContactAddress
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Category { get; set; }
        public string Tag { get; set; }

        public ICollection<ContactPhone> ContactPhones { get; set; }
        public ICollection<ContactEmail> ContactEmails { get; set; }
        public Guid ContactId { get; set; }

    }
}
