﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
    public class LockedItem
    {
        public Guid Id { get; set; }
        public string User { get; set; }
        public Guid ItemId { get; set; }
    }
}
