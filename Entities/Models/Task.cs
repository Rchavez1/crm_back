﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("task")]
    public class Task
    {
        public Guid Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Title { get; set; }
        public string Priority { get; set; }
        public string Status { get; set; }


        public ICollection<Activity> Activities { get; set; }

        [ForeignKey(nameof(Contact))]
        public Guid ContactId { get; set; }
        public Contact Contact { get; set; }
    }
}
