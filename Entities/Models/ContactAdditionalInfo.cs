﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("contactAdditionalInfo")]
    public class ContactAdditionalInfo
    {
        public Guid Id { get; set; }
        public string Remarks { get; set; }
        public string WebsiteURL { get; set; }
        public string FacebookURL { get; set; }
        public string InstagramURL { get; set; }

        public Guid ContactId { get; set; }
    }
}
