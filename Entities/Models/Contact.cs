﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("contact")]
    public class Contact
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DoB { get; set; }
        public string Company { get; set; }
        public string Nationality { get; set; }
        public string Language { get; set; }
        public string Department { get; set; }
        public string WeekDays { get; set; }
        public string RangeHours { get; set; }

        public ICollection<ContactAddress> ContactAddresses { get; set; }
        public ICollection<Task> Tasks { get; set; }
        public LockedItem LockedItem { get; set; }
        public ContactAdditionalInfo ContactAdditionalInfo { get; set; }
    }
}
