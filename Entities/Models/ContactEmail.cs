﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("contactEmail")]
    public class ContactEmail
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string Email { get; set; }
        public DateTime DateCreated { get; set; }
        public string UserEntry { get; set; }
        public string UserEdit { get; set; }

        [ForeignKey(nameof(ContactAddress))]
        public Guid ContactAddressId { get; set; }
        public ContactAddress ContactAddress { get; set; }
    }
}
