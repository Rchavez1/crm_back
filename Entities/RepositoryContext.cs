﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class RepositoryContext: DbContext
    {
        public RepositoryContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ContactAddress> ContactAddresses { get; set; }
        public DbSet<ContactEmail> ContactEmails { get; set; }
        public DbSet<ContactPhone> ContactPhones { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<LockedItem> LockedItems { get; set; }
        public DbSet<ContactAdditionalInfo> ContactAdditionalInfos { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Contact>()
            .HasOne(p => p.LockedItem)
            .WithOne()
            .OnDelete(DeleteBehavior.Cascade)
            .HasForeignKey<LockedItem>(l=>l.ItemId);

            builder.Entity<Contact>()
                .HasMany(a => a.ContactAddresses)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Contact>()
            .HasOne(a => a.ContactAdditionalInfo)
            .WithOne()
            .OnDelete(DeleteBehavior.Cascade)
            .HasForeignKey<ContactAdditionalInfo>(l => l.ContactId);
        }

        }
}
