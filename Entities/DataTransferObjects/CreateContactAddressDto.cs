﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DataTransferObjects
{
    public class CreateContactAddressDto
    {
        public string Type { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public ICollection<ContactPhone> ContactPhones { get; set; }
        public ICollection<ContactEmail> ContactEmails { get; set; }
        public Guid ContactId { get; set; }
    }
}
