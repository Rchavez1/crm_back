﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DataTransferObjects
{
    public class CreatePhoneDto
    {
        public string Type { get; set; }
        public string Phone { get; set; }
        public DateTime DateCreated { get; set; }
        public string UserEntry { get; set; }
    }
}
