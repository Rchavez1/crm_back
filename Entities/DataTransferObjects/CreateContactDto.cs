﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DataTransferObjects
{
    public class CreateContactDto
    {
        public string Type { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DoB { get; set; }
        public string Company { get; set; }
        public string Nationality { get; set; }
        public string Language { get; set; }
        public string Department { get; set; }
        public string WeekDays { get; set; }
        public string RangeHours { get; set; }
    }
}
