﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DataTransferObjects
{
    public class CreateContactAdditionalInfoDto
    {
        public string Remarks { get; set; }
        public string WebsiteURL { get; set; }
        public string FacebookURL { get; set; }
        public string InstagramURL { get; set; }
        public Guid ContactId { get; set; }
    }
}
